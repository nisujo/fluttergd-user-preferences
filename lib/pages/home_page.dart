import 'package:flutter/material.dart';
import 'package:user_preferences/preferences/user_preferences.dart';
import 'package:user_preferences/widgets/drawer_menu.dart';

class HomePage extends StatelessWidget {
  static final String routeName = 'home';
  final UserPreferences userPreferences = UserPreferences();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Preferencias de Usuario'),
        backgroundColor:
            userPreferences.secondaryColor ? Colors.teal : Colors.blue,
      ),
      drawer: DrawerMenu(),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _content('Color Secundario: ${userPreferences.secondaryColor}'),
            _content('Género: ${userPreferences.gender}'),
            _content('Nombre de Usuario: ${userPreferences.name}'),
          ],
        ),
      ),
    );
  }

  Widget _content(String value) {
    return Container(
      color: Colors.lightBlue[100],
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.symmetric(vertical: 10.0),
      child: Text(
        value,
        textAlign: TextAlign.center,
      ),
    );
  }
}
