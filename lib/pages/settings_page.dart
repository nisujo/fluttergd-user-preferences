import 'package:flutter/material.dart';
import 'package:user_preferences/preferences/user_preferences.dart';
import 'package:user_preferences/widgets/drawer_menu.dart';

class SettingsPage extends StatefulWidget {
  static final String routeName = 'settings';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final UserPreferences userPreferences = UserPreferences();

  bool _secondaryColor;
  int _gender;
  String _name;

  TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();

    _secondaryColor = userPreferences.secondaryColor;
    _gender = userPreferences.gender;
    _name = userPreferences.name;

    _textEditingController = TextEditingController(text: _name);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ajustes'),
        backgroundColor:
            userPreferences.secondaryColor ? Colors.teal : Colors.blue,
      ),
      drawer: DrawerMenu(),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.all(20.0),
            child: Text(
              'Container',
              style: TextStyle(
                fontSize: 45.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Divider(),
          SwitchListTile(
            title: Text('Color Secundario'),
            value: _secondaryColor,
            onChanged: setSecondaryColor,
          ),
          RadioListTile(
            title: Text('Masculino'),
            value: 1,
            groupValue: _gender,
            onChanged: setSelectedRadio,
          ),
          RadioListTile(
            title: Text('Femenino'),
            value: 2,
            groupValue: _gender,
            onChanged: setSelectedRadio,
          ),
          Divider(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              controller: _textEditingController,
              decoration: InputDecoration(
                labelText: 'Nombre',
                helperText: 'Nombre de la persona usando el teléfono',
              ),
              onChanged: setName,
            ),
          )
        ],
      ),
    );
  }

  void setSecondaryColor(bool value) {
    setState(() {
      _secondaryColor = value;
      userPreferences.secondaryColor = value;
    });
  }

  void setSelectedRadio(int value) {
    setState(() {
      _gender = value;
      userPreferences.gender = value;
    });
  }

  void setName(String value) {
    setState(() {
      _name = value;
      userPreferences.name = value;
    });
  }
}
