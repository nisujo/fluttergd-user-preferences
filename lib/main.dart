import 'package:flutter/material.dart';
import 'package:user_preferences/pages/home_page.dart';
import 'package:user_preferences/pages/settings_page.dart';
import 'package:user_preferences/preferences/user_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final UserPreferences userPreferences = UserPreferences();
  await userPreferences.initialize();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final UserPreferences userPreferences = UserPreferences();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Preferencias',
      debugShowCheckedModeBanner: false,
      initialRoute: userPreferences.initialPage,
      routes: {
        HomePage.routeName: (_) => HomePage(),
        SettingsPage.routeName: (_) => SettingsPage(),
      },
    );
  }
}
